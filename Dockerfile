FROM golang:1.10.1 AS build-env
ENV CGO_ENABLED 0
RUN go version
COPY . /go/src/go_star_wars
RUN go build go_star_wars/model
RUN go build go_star_wars/dao
RUN go build go_star_wars/controller
RUN go build go_star_wars/adapter
RUN go build -o /server go_star_wars

FROM alpine:3.7
RUN apk add --no-cache ca-certificates \
        dpkg \
        gcc \
        git \
        musl-dev \
        bash
WORKDIR /
COPY --from=build-env /server /
ENV PORT 6000
EXPOSE 6000
CMD ["./server"]