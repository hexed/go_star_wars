package models

type FilmsList struct {
	Films	[]string	`bson:"films" json:"films"`
}

type IntegrationResponse struct {
	Results	[]FilmsList	`bson:"results" json:"results"`
}