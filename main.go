package main

import (
	"os"
	"log"
	"net/http"
  "github.com/gorilla/mux"
  "gopkg.in/mgo.v2"
	planetcontroller "go_star_wars/controller"
	adapter "go_star_wars/adapter"
)

func main() {
  db, err := mgo.Dial("mongodb:27017")
  if err != nil {
    log.Fatal("cannot dial mongo", err)
  }
  defer db.Close()

	port := os.Getenv("PORT")
	if port == "" {
		log.Fatal("PORT environment variable was not set")
	}

  router := mux.NewRouter()
	router.Handle("/planets", adapter.Adapt(http.HandlerFunc(planetcontroller.Index), adapter.WithDB(db))).
				 Methods("GET")
	router.Handle("/planets/{id}", adapter.Adapt(http.HandlerFunc(planetcontroller.Show), adapter.WithDB(db))).
				 Methods("GET")
	router.Handle("/planets", adapter.Adapt(http.HandlerFunc(planetcontroller.Create), adapter.WithDB(db))).
				 Methods("POST")
	router.Handle("/planets/{id}", adapter.Adapt(http.HandlerFunc(planetcontroller.Delete), adapter.WithDB(db))).
				 Methods("DELETE")
	
	log.Fatal(http.ListenAndServe(":" + port, router))
}