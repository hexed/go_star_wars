package test
import (
  "log"
  "net/http"
  "net/http/httptest"
  "encoding/json"
  "bytes"
  "gopkg.in/mgo.v2"
  "strings"
  model "go_star_wars/model"
  adapter "go_star_wars/adapter"
  planetcontroller "go_star_wars/controller"
  "testing"
)

var planet model.Planet

func TestCreate(t *testing.T) {
  db, err := mgo.Dial("localhost:27017")
  if err != nil {
    log.Fatal("cannot dial mongo", err)
  }
  defer db.Close()
  var response_json = `{ "name": "Dagobah", "climate": "murky", "terrain": "swamp, jungles", "count_films":3 }`
  var jsonStr = []byte(response_json)
  req, err := http.NewRequest("POST", "/planets", bytes.NewBuffer(jsonStr))
  if err != nil {
    t.Fatal(err)
  }
  req.Header.Set("Content-Type", "application/json")
  rr := httptest.NewRecorder()
  handler := adapter.Adapt(http.HandlerFunc(planetcontroller.Create), adapter.WithDB(db))
  handler.ServeHTTP(rr, req)
  if status := rr.Code; status != http.StatusOK {
    t.Errorf("handler returned wrong status code: got %v want %v",
        status, http.StatusOK)
  }
  in := []byte(rr.Body.String())  
  var returned_planet model.Planet
  json.Unmarshal(in, &returned_planet)  

  var expected = "Dagobah"
  var expected2 = 3
  if returned_planet.Name != expected && returned_planet.FilmsCount != expected2 {
    t.Errorf("handler returned unexpected body: got %v want %v",
        rr.Body.String(), expected)
  }
  planet = returned_planet
}

func TestIndex(t *testing.T) {
  db, err := mgo.Dial("localhost:27017")
  if err != nil {
    log.Fatal("cannot dial mongo", err)
  }
  defer db.Close()

  req, err := http.NewRequest("GET", "/planets", nil)
  if err != nil {
    t.Fatal(err)
  }
  rr := httptest.NewRecorder()
  handler := adapter.Adapt(http.HandlerFunc(planetcontroller.Index), adapter.WithDB(db))
  handler.ServeHTTP(rr, req)
  if status := rr.Code; status != http.StatusOK {
    t.Errorf("handler returned wrong status code: got %v want %v",
        status, http.StatusOK)
  }

  var expected = "Dagobah"
  if !strings.Contains(rr.Body.String(), expected) {
      t.Errorf("handler returned unexpected body: got %v want %v",
          rr.Body.String(), expected)
  }
}

func TestShow(t *testing.T) {
  db, err := mgo.Dial("localhost:27017")
  if err != nil {
    log.Fatal("cannot dial mongo", err)
  }
  defer db.Close()

  var url = "/planets/" + planet.ID.Hex()
  req, err := http.NewRequest("GET", url, nil)
  if err != nil {
    t.Fatal(err)
  }
  rr := httptest.NewRecorder()
  handler := adapter.Adapt(http.HandlerFunc(planetcontroller.Index), adapter.WithDB(db))
  handler.ServeHTTP(rr, req)
  if status := rr.Code; status != http.StatusOK {
    t.Errorf("handler returned wrong status code: got %v want %v",
        status, http.StatusOK)
  }
  var expected = planet.ID.Hex()
  if !strings.Contains(rr.Body.String(), expected) {
    t.Errorf("handler returned unexpected body: got %v want %v",
        rr.Body.String(), expected)
  }
}

func TestDelete(t *testing.T) {
  db, err := mgo.Dial("localhost:27017")
  if err != nil {
    log.Fatal("cannot dial mongo", err)
  }
  defer db.Close()

  var url = "/planets/" + planet.ID.Hex()
  req, err := http.NewRequest("DELETE", url, nil)
  if err != nil {
    t.Fatal(err)
  }
  rr := httptest.NewRecorder()
  handler := adapter.Adapt(http.HandlerFunc(planetcontroller.Delete), adapter.WithDB(db))
  handler.ServeHTTP(rr, req)
  if status := rr.Code; status != http.StatusNoContent {
    t.Errorf("handler returned wrong status code: got %v want %v",
        status, http.StatusNoContent)
  }
}