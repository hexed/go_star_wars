package planetcontroller

import (
	"encoding/json"
  "net/http"
  "io/ioutil"
  "github.com/gorilla/mux"
  "github.com/gorilla/context"
  model "go_star_wars/model"
	"go_star_wars/dao"
  "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var planetdao = dao.PlanetsDAO{}

func respondWithError(w http.ResponseWriter, code int, msg string) {
	respondWithJson(w, code, map[string]string{"error": msg})
}

func respondWithJson(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}

func Index(w http.ResponseWriter, r *http.Request) {
	params := r.URL.Query()
	var query = bson.M{}
	var name = params["name"]
	if name != nil {
		query = bson.M{"$text": bson.M{"$search": name[0]}}
  }
  db := context.Get(r, "database").(*mgo.Session)
  planets, err := planetdao.FindAll(db, query)
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondWithJson(w, http.StatusOK, planets)
}

func Show(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
  var id = params["id"]
  db := context.Get(r, "database").(*mgo.Session)
  planet, err := planetdao.FindById(db, id)
  if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
  }
  if planet.ID == "" {
    respondWithError(w, http.StatusNotFound, `Não Encontrado :(` )
    return
  }
	respondWithJson(w, http.StatusOK, planet)
}

func Create(w http.ResponseWriter, r *http.Request) {
  var planet model.Planet
  db := context.Get(r, "database").(*mgo.Session)
  if err := json.NewDecoder(r.Body).Decode(&planet); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
  }
	if planet.Name != "" {
		var url = "https://swapi.co/api/planets/?search="+planet.Name
		response, _ := http.Get(url)
		if response != nil {
			responseData, err := ioutil.ReadAll(response.Body)
			if err == nil {
				var responseObject model.IntegrationResponse
				json.Unmarshal(responseData, &responseObject)
				if responseObject.Results != nil {
					planet.FilmsCount = len(responseObject.Results[0].Films)
				}
			}
		}
	}

  planet.ID = bson.NewObjectId()

  err := planetdao.Save(db, planet)
  if err != nil {
    respondWithError(w, http.StatusInternalServerError, err.Error())
		return
  }
  respondWithJson(w, http.StatusOK, planet)
  return
}

func Delete(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	var id = params["id"]
  db := context.Get(r, "database").(*mgo.Session)
  err := planetdao.Destroy(db, id)
  if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
  }
  respondWithJson(w, http.StatusNoContent, "")
}