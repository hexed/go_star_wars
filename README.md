# Golang Star Wars!

### Como configurar seu ambiente

* baixar o projeto
* baixar o docker
* adicionar o projeto no seu ambiente go
* Iniciar os containers

```
docker-compose build .
```

### Iniciando o projeto

```
docker-compose up
```

### Endpoints

Pode utilizar o GoStarWars.postman_collection.json que está na raiz do projeto para acessar os endpoints disponibilizados pela API.

### Rodando os testes

Na pasta "/test" do projeto, executar o comando:

```
go test
```