package dao

import (
  "fmt"
  model "go_star_wars/model"
  "gopkg.in/mgo.v2"
  "gopkg.in/mgo.v2/bson"  
)

type PlanetsDAO struct {}

const (
	DATABASE = "gostarwars"
	COLLECTION = "planets"
)

func (m *PlanetsDAO) FindAll(db *mgo.Session, query bson.M) (planets []model.Planet, err error){
	index := mgo.Index{
    	Key: []string{"$text:name"},
  }
  var collection = db.DB(DATABASE).C(COLLECTION)
  collection.EnsureIndex(index)
  err = collection.Find(query).All(&planets)
	return planets, err
}

func (m *PlanetsDAO) FindById(db *mgo.Session, id string) (planet model.Planet, err error){
  defer func() {
		if err := recover(); err != nil {
      fmt.Println(err)      
		}
	}()
	err = db.DB(DATABASE).C(COLLECTION).FindId(bson.ObjectIdHex(id)).One(&planet)
	return planet, err
}

func (m *PlanetsDAO) Save(db *mgo.Session, planet model.Planet) (err error) {
	err = db.DB(DATABASE).C(COLLECTION).Insert(&planet)
	return err
}

func (m *PlanetsDAO) Destroy(db *mgo.Session, id string) (err error) {
  defer func() {
		if err := recover(); err != nil {
      fmt.Println(err)      
		}
	}()
	err = db.DB(DATABASE).C(COLLECTION).RemoveId(bson.ObjectIdHex(id))
	return err
}