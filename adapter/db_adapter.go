package adapter

import (
	"gopkg.in/mgo.v2"
	"net/http"
	"github.com/gorilla/context"
)

type Adapter func(http.Handler) http.Handler

func Adapt(h http.Handler, adapters ...Adapter) http.Handler {
	for _, adapter := range adapters {
		h = adapter(h)
	}
	return h
}

func WithDB(db *mgo.Session) Adapter {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			dbsession := db.Copy()
			defer dbsession.Close()
			context.Set(r, "database", dbsession)
			h.ServeHTTP(w, r)

		})
	}
}